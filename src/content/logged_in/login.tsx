import React, {  Component } from "react";
import {Redirect} from 'react-router-dom'
import cookie from 'react-cookies'

// handles the steam oaugh and redirect
// https://github.com/fedebabrauskas/mern-steam-jwt-login/blob/master/client/src/App.js

interface Login_State {
    verified: boolean;
    backend?: string
}
interface Login_Props {
    setUser?: any
    backend?: string
}
export class Login extends Component <Login_Props, Login_State>{
    constructor(props: any) {
        super(props);
        this.state = {
            verified: false,
            backend: this.props.backend,
        }
    }

    componentDidMount = async () => {
        let url = `${this.state.backend}/account`
        let result: Response = await fetch(url, {
            method: 'GET',
            credentials: 'include',
            //mode: "no-cors"
        })
            .catch((err)=> {console.log(err);return err})

        if(result.status === 200){
            this.props.setUser(await result.json())
        }

        if(result.status === 401){
            this.props.setUser(undefined)
            // the cookie is set on login
            cookie.remove("loggedIn")
        }
    }

    render() {
        if(cookie.load("loggedIn")){
            return <Redirect to='/account' />
        }

        return <div className="App">
            <h1>Login</h1>
            <a href={`${this.state.backend}/auth/steam`}>
                <img
                    //onClick={handleLogin}
                    //   https://community.cloudflare.steamstatic.com/public/shared/images/signinthroughsteam/sits_landing.png
                    //src="https://steamcommunity-a.akamaihd.net/public/images/signinthroughsteam/sits_01.png"
                    src={"https://community.cloudflare.steamstatic.com/public/shared/images/signinthroughsteam/sits_landing.png"}
                    alt="Login with Steam"
                />
            </a>
        </div>
    }
}