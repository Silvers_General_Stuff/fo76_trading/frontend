// redirected here after login

import React, {Component} from "react";
import {
    Button,
    StructuredListBody,
    StructuredListCell,
    StructuredListHead,
    StructuredListRow,
    StructuredListWrapper,
    TextInput, Dropdown
} from 'carbon-components-react';
import reddit_icon from "../../images/Reddit_Lockup_OnDark.png"
import {DB, User, Data_User_Account_Data} from "../../interfaces/interfaces";


interface Account_Props {
    user?: User;
    db: DB
    backend?: string
}
interface Account_State {
    user?: User;
    user_data?: Data_User_Account_Data[]
    db: DB
    name?: string
    platform?: string
    backend?: string
}

export class Account extends Component <Account_Props, Account_State>{
    constructor(props: Account_Props) {
        super(props);
        this.state = {
            user: this.props.user,
            user_data: undefined,
            db: this.props.db,
            name: undefined,
            platform: undefined,
            backend: this.props.backend,
        }
    }

    componentDidUpdate(prevProps: Account_Props) {
        if(typeof prevProps.user === "undefined" && typeof this.props.user !== "undefined"){
            this.setState({user: this.props.user})
        }
    }

    componentDidMount = async () => {
        this.account_get()
    }

    account_get = () =>{
        fetch(`${this.state.backend}/account`, {method: 'GET', credentials: 'include'})
            // convert
            .then(async (response) => await response.json())
            .then(user => this.setState({user_data: user}))
            .catch(err => {console.log(err)})
    }

    account_unlink =(url: string)=>{
        fetch(url, {method: 'DELETE', credentials: 'include'})
            // convert
            .then(() => this.account_get())
            .catch(err => {console.log(err)})

    }

    account_name_update = () => {
        let {name} = this.state
        if(!name){return}
        fetch(`${this.state.backend}/account`, {
            method: 'POST',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({fo76_name: name})
        })
            .then(() => this.account_get())
            .catch(err => {console.log(err)})
    }
    account_platform_update  = () => {
        let {platform} = this.state
        if(!platform){return}
        fetch(`${this.state.backend}/account`, {
            method: 'POST',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({platform: platform})
        })
            .then(() => this.account_get())
            .catch(err => {console.log(err)})

    }


    create_table = (user_data: Data_User_Account_Data)=>{
        let discord_name, discord_date, reddit_name, reddit_date

        if(!!user_data.discord){
            discord_name = user_data.discord.username
            discord_date = user_data.discord.first_added
        }
        if(!!user_data.reddit){
            reddit_name = user_data.reddit.name
            reddit_date = user_data.reddit.first_added
        }

        let platforms = [
            { id: "PC", text:"PC" },
            { id: "PS", text:"PS" },
            { id: "Xbox", text:"Xbox" },
        ]

        return <StructuredListWrapper ariaLabel="Structured list">
            <StructuredListHead>
                <StructuredListRow head tabIndex={0}>
                    <StructuredListCell head>Platform</StructuredListCell>
                    <StructuredListCell head>Name</StructuredListCell>
                    <StructuredListCell head>Misc</StructuredListCell>
                    <StructuredListCell head>Link/Unlink/Update</StructuredListCell>
                </StructuredListRow>
            </StructuredListHead>
            <StructuredListBody>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>Steam/This Site</StructuredListCell>
                    <StructuredListCell>{user_data.displayName}</StructuredListCell>
                    <StructuredListCell>{user_data.first_added}</StructuredListCell>
                    <StructuredListCell/>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>Owns Steam Game?</StructuredListCell>
                    <StructuredListCell>{user_data.has_game.toString()}</StructuredListCell>
                    <StructuredListCell/>
                    <StructuredListCell/>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>Platform</StructuredListCell>
                    <StructuredListCell>
                        <Dropdown
                            inline
                            id={"Dropdown"}
                            titleText={""}
                            ariaLabel={"misc"}
                            label={this.state.platform || user_data.platform}
                            items={platforms}
                            itemToString={(item) => (item ? item.text : '')}
                            onChange={
                                ({ selectedItem }) => {
                                    if(selectedItem){
                                        this.setState({platform:selectedItem.text})
                                    }
                                }
                            }
                        />
                    </StructuredListCell>
                    <StructuredListCell>
                        {user_data.platform || ""}
                    </StructuredListCell>
                    <StructuredListCell>
                        <Button
                            kind="primary"
                            size='small'
                            tabIndex={0}
                            type="submit"
                            onClick={()=>{this.account_platform_update()}}
                        >
                            Update
                        </Button>
                    </StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                <StructuredListCell>Fallout76</StructuredListCell>
                <StructuredListCell>
                    <TextInput
                        helperText={false}
                        id="NameChanger"
                        placeholder={user_data.fo76_name}
                        labelText={false}
                        value={this.state.name || user_data.fo76_name}
                        onChange={(event) => {
                            this.setState({name: event.target.value});
                        }}
                    />
                </StructuredListCell>
                <StructuredListCell>
                        {
                            user_data.fo76_names_past[user_data.fo76_names_past.length-1] || ""
                            //user_data.fo76_names_past.join("\n")
                        }
                </StructuredListCell>
                <StructuredListCell>
                    <Button
                        kind="primary"
                        size='small'
                        tabIndex={0}
                        type="submit"
                        onClick={()=>{this.account_name_update()}}
                    >
                        Update
                    </Button>
                </StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>Discord</StructuredListCell>
                    <StructuredListCell>{discord_name}</StructuredListCell>
                    <StructuredListCell>{discord_date}</StructuredListCell>
                    <StructuredListCell>{
                        this.get_oauth_icon(
                            "Discord",
                            `${this.state.backend}/auth/discord`,
                            {
                                url: "https://discord.com/assets/e7a3b51fdac2aa5ec71975d257d5c405.png",
                                height: 50,
                                width: 150
                            },
                            discord_name,
                        )
                    }
                    </StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>Reddit</StructuredListCell>
                    <StructuredListCell>{reddit_name}</StructuredListCell>
                    <StructuredListCell>{reddit_date}</StructuredListCell>
                    <StructuredListCell>{
                        this.get_oauth_icon(
                            "Reddit",
                            `${this.state.backend}/auth/reddit`,
                            {
                                url: reddit_icon,
                                //url: "https://www.redditinc.com/assets/images/site/logo.svg",
                                height: 50,
                                width: 150
                            },
                            reddit_name,
                        )
                    }
                    </StructuredListCell>
                </StructuredListRow>
            </StructuredListBody>
        </StructuredListWrapper>

    }

    get_oauth_icon = (platform: string, link: string,  image: {url: string; height: number; width: number},name?: string ) =>{
        if(typeof name === "undefined"){
            if(!link){return null}

            return <a href={link}>
                <img
                    src={image.url}
                    alt={`Login via ${platform}`}
                    height={image.height}
                    width={image.width}
                />
            </a>
        }else{
            if(link){
                return <Button kind='danger' size='small' onClick={()=>{this.account_unlink(link)}}>Unlink</Button>
            }else{
                return null
            }
        }
    }

    render() {
        console.log(this.state.platform)
        let {user_data} = this.state
        if(!user_data){return null}

        let table = this.create_table(user_data[0])

        return <div className="bx--grid bx--grid--full-width main_body">
            <div className="bx--row main_body__banner">
                <div className="bx--col">
                    <h1 className="main_body__heading">
                        Account
                    </h1>
                </div>
            </div>
            <div className="bx--row main_body__r2">
                <div className="bx--col">
                    <h3 className="main_body__subheading">
                        For linking other services.
                    </h3>
                </div>
            </div>
            <div className="bx--row main_body__r3 ">
                <div className="bx--col" style={{maxWidth: "1000px"}}>
                    {table}
                </div>
            </div>
        </div>
    }
}