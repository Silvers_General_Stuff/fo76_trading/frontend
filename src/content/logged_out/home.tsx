import React, { Component } from "react";
import { Accordion, AccordionItem } from "carbon-components-react";

export class Home extends Component <any, any>{
    render() {
        return <div className="bx--grid bx--grid--full-width main_body">
            <div className="bx--row main_body__banner">
                <div className="bx--col bx--no-gutter">
                    <h1>
                        Yet Another Fo76 Trading Site
                    </h1>
                    <h3>
                        (YAF76TS)
                    </h3>
                    <h5>
                        (squint and it looks like "76 VATS")
                    </h5>
                </div>
            </div>
            <div className="bx--row main_body__r2">
                <div className="bx--col bx--no-gutter">
                    <Accordion align='start'>
                        <AccordionItem open title="Why?">
                            <p>
                                A long time ago in a discord far, far away....
                                <br />
                                I was trying to find an Unyielding/Any/Weapon Weight reduction armor.
                                <br />
                                So I checked the trade channel.
                                <br/>
                                There were imgur albums, text posts, an array of spreadsheets.
                                <br />
                                Everyone using different shorthand and none of it searchable.
                                <br />
                                Basically a nightmare to find stuff.
                                <br />
                                So I decided to over-engineer a solution.
                            </p>
                        </AccordionItem>
                        <AccordionItem title="Why??">
                            <p>
                                Why Not?
                            </p>
                        </AccordionItem>
                        <AccordionItem title="Why???">
                            <p>
                                Fine, technical answer then.
                                <br />
                                I wanted to experiment with using both Oauth, Docker and Postgres as well as something that looks better than Bootstrap.
                                <br />

                                To replace Bootstrap I settled on
                                <a href={"https://www.carbondesignsystem.com/"} target={"_blank"} rel={"noreferrer noopener"} > IBM's Carbon Design</a>
                                along with the
                                <a href={"https://github.com/carbon-design-system/carbon-addons-iot-react"} target={"_blank"} rel={"noreferrer noopener"} > IOT addon</a> which extends the carbon components.

                                <br />

                                All the code is available
                                <a href={"https://gitlab.com/Silvers_General_Stuff/fo76_trading"} target={"_blank"} rel={"noreferrer noopener"} > on my gitlab.</a>
                                if you want to check it out.

                                <br />

                                So far it was fun building this and putting everything together.
                            </p>
                        </AccordionItem>
                        <AccordionItem title="Advantages of this site?">
                            <p>
                                None!

                                <br />
                                This is how I wanted to see the data, searchable as well as being able to see a seller's reputation.
                                <br />
                                Signing in via steam so its easy, and no passwords to manage/remember.
                                <br />
                                Linking to both reddit and Discord to be able to use systems folks already have in place.
                            </p>
                        </AccordionItem>
                        <AccordionItem title="What isn't ready?">
                            <p>
                                Currently only weapons and armor are listable, working on how to do items/plans next.
                            </p>
                        </AccordionItem>
                        <AccordionItem title="There are bugs?">
                            <p>
                                Please open an issue on
                                <a href={"https://gitlab.com/groups/Silvers_General_Stuff/fo76_trading/-/issues"} target={"_blank"} rel={"noreferrer noopener"} > the gitlab.</a>
                            </p>
                        </AccordionItem>
                        <AccordionItem title="Who are you?">
                            <p>
                                On discord I am Silver#5563
                                <br />
                                On this site I am just a
                                <a href={`#/user/76561198033798586`} target={"_blank"} rel={"noreferrer noopener"} > poor trader.</a>
                            </p>
                        </AccordionItem>
                    </Accordion>

                </div>
            </div>
        </div>
    }
}