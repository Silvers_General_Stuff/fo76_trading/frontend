import React, {Component} from "react";
import {DataTableManager, DataTableManager_Object_Header, DataTableManager_Object_Row, process_data_listings_general, DataTableManager_Object_Row_values} from "../../functions/functions_react";
import {Button} from "carbon-components-react";
import {DB, Effect_Object, Legendary_Object, Listing_Object, Offers_Object, User, Wanted} from "../../interfaces/interfaces";

interface Listing_Offers_Props {
    user?: User;
    db: DB
    backend?: string
}
interface Listing_Offers_State {
    user?: User;
    db: DB
    data_listing: Listing_Object[]
    data_legendary: Legendary_Object[]
    data_effect: Effect_Object[]
    data_offers_my: Offers_Object[]
    backend?: string
    [propName: string]: any;
}

export class Listings extends Component <Listing_Offers_Props, Listing_Offers_State>{
    constructor(props: Listing_Offers_Props) {
        super(props);
        this.state = {
            user: this.props.user,
            db: this.props.db,
            data_listing: [],
            data_legendary: [],
            data_effect: [],
            data_offers_my: [],
            backend: this.props.backend,
        }
    }

    componentDidUpdate(prevProps: Listing_Offers_Props) {
        if(typeof prevProps.user === "undefined" && typeof this.props.user !== "undefined"){
            this.setState({user: this.props.user})

            // set teh offer data
            this.set_data(this.state.db, `${this.state.backend}/offers/my`, "data_offers_my", [])
        }
    }

    componentDidMount = () => {
        let {db} = this.state

        // populate initial data
        // get initial listings
        this.set_data(db,`${this.state.backend}/listing`, "data_listing", [])
        // get legendary data
        this.set_data(db, `${this.state.backend}/legendary/item`, "data_legendary", [])
        // get legendary effects
        this.set_data(db, `${this.state.backend}/legendary/effect`, "data_effect", [])
    }

    // create searchbar

    set_data = async(db: DB, url: string, category: "data_legendary" | "data_effect"| "data_listing" | "data_offers_my" , default_response:any = []) =>{
        //pull initially from cache
        await db.getAll(category).then(data => this.setState({[category]:data}))

        // fetch fresh data
        await fetch(url, {method: 'GET', credentials: 'include'})
            // convert
            .then(async (response) => await response.json())
            .then(data => {
                // update teh state
                this.setState({[category]:data})
                return data
            })
            .then(async(data: Legendary_Object[] | Effect_Object[] | Listing_Object[] | Offers_Object[])=>{
                if(
                    category === "data_listing" ||
                    category === "data_offers_my"
                ){
                    await db.clear(category)
                }

                for(let i=0;i<data.length;i++){
                    await db.put(category, data[i], data[i].id);
                }
            })
            .catch((err)=> {
                console.log(err);
                this.setState({[category]:default_response})
            })

    }


    processTableData = (): [DataTableManager_Object_Row[], DataTableManager_Object_Header[]] =>{
        // raw data
        let {data_listing, data_legendary, data_effect, user, data_offers_my} = this.state
        let headers_tmp:{ [propName: string]: DataTableManager_Object_Header } = {
            seller_name :{
                filter: {
                    placeholderText: 'Sellers'
                },
                id: 'seller_name',
                isSortable: true,
                name: 'Seller',
                renderDataFunction: (rowData: any) =>{return <a href={`#/user/${rowData.row.seller}`} style={{"textDecoration": "none", color: "#f4f4f4"}} ><b>{rowData.row.seller_name}</b></a>}
            },
            platform:{
                filter: {
                    isMultiselect: true,
                    options: [
                        {id: 'PC', text: 'PC'},
                        {id: 'PS', text: 'PS'},
                        {id: 'Xbox', text: 'Xbox'}
                    ],
                    placeholderText: 'Platform'
                },
                id: 'platform',
                isSortable: true,
                name: 'Platform'
            },
            start:{
                id: 'start',
                isSortable: true,
                name: 'Start'
            },
            wanted:{
                filter: {
                    placeholderText: 'Caps/Wishlist',
                    filterFunction: (columnFilterValue:Wanted[], currentValue: any) => {
                        let value = false
                        columnFilterValue.forEach(
                            item => {
                                if(item.type.indexOf(currentValue) !== -1){
                                    value = true
                                }
                            })
                        return value
                    }
                },
                id: 'wanted',
                isSortable: false,
                name: 'Wanted',
                renderDataFunction: (rowData: { value: Wanted[], row: DataTableManager_Object_Row_values }) =>{
                    let result_array: any[] = []
                    let wanted = rowData.value

                    // <a href={`#/user/${rowData.row.seller}`} style={{"textDecoration": "none", color: "#f4f4f4"}} ><b>{rowData.row.seller_name}</b></a>
                    for(let i=0;i<wanted.length;i++){
                        if(wanted[i].type === "caps"){
                            result_array.push(`Caps: ${wanted[i].value}`)
                            result_array.push("\n")
                        }

                        if(wanted[i].type === "wishlist" && wanted[i].value){
                            result_array.push(<a href={`#/user/${rowData.row.seller}/wishlist`} style={{"textDecoration": "none", color: "#f4f4f4"}} ><b>Wishlist</b></a>)
                        }

                    }

                    return <pre>
                        {result_array}
                    </pre>

                }
            },
            note:{
                filter: {
                    placeholderText: 'Note'
                },
                id: 'note',
                isSortable: true,
                name: 'Note'
            },
            level:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'level',
                isSortable: true,
                name: 'Level'
            },
            category:{
                filter: {
                    isMultiselect: true,
                    options: [
                        {id: 'Armor', text: 'Armor'},
                        {id: 'Weapon', text: 'Weapon'}
                    ],
                    placeholderText: 'Search                     '
                },
                id: 'category',
                isSortable: true,
                name: 'Category'
            },
            type:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search                     '
                },
                id: 'type',
                isSortable: true,
                name: 'Type'
            },
            type_sub:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'type_sub',
                isSortable: true,
                name: 'Type-Sub'
            },
            category_specific:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'category_specific',
                isSortable: true,
                name: 'Type-Sub-Sub'
            },
            unique_name:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'unique_name',
                isSortable: true,
                name: 'Unique Name'
            },
            effect_0:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'effect_0',
                isSortable: true,
                name: 'Effect 0'
            },
            effect_1:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'effect_1',
                isSortable: true,
                name: 'Effect 1'
            },
            effect_2:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'effect_2',
                isSortable: true,
                name: 'Effect 2'
            },
            effect_3:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'effect_3',
                isSortable: true,
                name: 'Effect 3'
            },
            /*
            effect_4:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'effect_4',
                isSortable: true,
                name: 'Effect 4'
            },
            effect_5:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'effect_5',
                isSortable: true,
                name: 'Effect 5'
            },
             */
        }

        let [result, headers] = process_data_listings_general(headers_tmp,data_listing, data_legendary, data_effect)

        if(typeof this.state.user !== "undefined"){
            headers.push({
                id: 'bid',
                isSortable: false,
                name: 'Offer',
                renderDataFunction: (rowData: any) =>{
                    if(typeof user === "undefined"){return null}
                    if(rowData.row.seller !== user.id){
                        // check if the listing_id is not in teh data_offers_my

                        let existing = data_offers_my.filter(offer => offer.listing.id === rowData.rowId)
                        if(existing.length === 0){
                            return <Button kind='primary' size='small' onClick={()=>{this.offer_make(rowData.rowId)}} >Make Offer</Button>
                        }else{
                            return <Button kind='danger' size='small' onClick={()=>{this.offer_delete(rowData.rowId)}}>Cancel Offer</Button>
                        }


                    }
                }
            })
        }

        return [result, headers]
    }

    offer_make = async (listing_id: string) => {
        fetch(`${this.state.backend}/offers`, {
            method: 'POST', credentials: 'include',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({listing_id: listing_id})
        })
            .then(async response => await response.json())
            .then(result => {
                if(result.success){
                    let {data_offers_my} = this.state
                    let tmp_offer:  Offers_Object = {
                        date: new Date().toISOString(),
                        id: 0,
                        listing: {
                            id: listing_id,
                            item: { md5: "" },
                            start: new Date().toISOString(),
                            status: "Draft",
                            quantity:1,
                            wanted: [],
                            note: ""
                        }
                    }
                    data_offers_my.push(tmp_offer)
                    this.setState({data_offers_my: data_offers_my})
                }
            })
            .catch(err => console.log(err))

    }

    offer_delete = async (listing_id: string) => {
        fetch(`${this.state.backend}/offers`, {
            method: 'DELETE', credentials: 'include',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({listing_id: listing_id})
        })
            .then(async response => await response.json())
            .then(result => {
                if(result.success){
                    let {data_offers_my} = this.state
                    let cleared = data_offers_my.filter(offer => offer.listing.id !== listing_id)
                    this.setState({data_offers_my: cleared})
                }
            })
            .catch(err => console.log(err))
    }

    render() {
        let [rows, headers] = this.processTableData()


        let view = {
            filters: [],
            pagination: {
                //maxPages: 5,
                page: 1,
                pageSize: 30,
                pageSizes: [
                    10,
                    20,
                    30
                ],
                //totalItems: 100
            },
            table: {
                ordering: [],
                sort: undefined
            },
            toolbar: {
                activeBar: 'filter'
            }
        }

        return <div className="bx--grid bx--grid--full-width main_body">
            <div className="bx--row main_body__banner">
                <h1 className="main_body__heading">
                    Listings
                </h1>
            </div>
            <div className="bx--row main_body__r2">
                <div className="bx--col bx--no-gutter">
                    <DataTableManager rows={rows} headers={headers} view={view} />
                </div>
            </div>
        </div>
    }
}