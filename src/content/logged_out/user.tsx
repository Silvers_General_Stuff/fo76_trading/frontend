import React, {Component} from "react";
import {
    DataTableManager,
    DataTableManager_Object_Header, DataTableManager_Object_Row,
    DataTableManager_Object_Row_values,
    process_data_listings_general,
} from "../../functions/functions_react";
import {
    Dropdown, ModalWrapper,
    StructuredListBody,
    StructuredListCell,
    StructuredListHead,
    StructuredListRow,
    StructuredListWrapper,
    TextInput,
    NumberInput,
    Tabs,
    Tab, Button
} from "carbon-components-react";
import {compare, timeAgo} from "../../functions/functions";
import {DB, Effect_Object, Legendary_Object, Listing_Object, Offers_Object, User, Wanted, Submitted_Object, Data_User_Account_Data} from "../../interfaces/interfaces";

interface User_Data_Props {
    user?: User;
    db: DB
    backend?: string
    [propName: string]: any;
}
interface User_Data_State {
    user?: User;
    db: DB
    user_id?: string
    section?:string
    data_legendary: Legendary_Object[]
    data_effect: Effect_Object[]
    data_offers_my: Offers_Object[]
    data_dropdown?: Weapon_Modal_data_dropdown
    data_user?: Data_User
    backend?: string
    [propName: string]: any;
}
interface Buy_Sell<T> {
    buy: T[]
    sell: T[]
}
interface Data_User {
    account_Data: Data_User_Account_Data[]
    current: Buy_Sell<Listing_Object>
    history: Buy_Sell<Listing_Object>
    pending: Buy_Sell<Offers_Object>
}
export class UserData extends Component <User_Data_Props, User_Data_State>{
    constructor(props: User_Data_Props) {
        super(props);
        let user_id, section

        if(this.props.match && this.props.match.params){
            if(this.props.match.params.userId){
                user_id = this.props.match.params.userId
            }
            if(this.props.match.params.section){
                section = this.props.match.params.section
            }
        }

        this.state = {
            user: this.props.user,
            db: this.props.db,
            user_id: user_id,
            section: section,
            data_legendary: [],
            data_effect: [],
            data_offers_my: [],
            data_dropdown: undefined,
            data_user:undefined,
            backend: this.props.backend,
        }
    }

    componentDidUpdate(prevProps: User_Data_Props) {
        if(typeof prevProps.user === "undefined" && typeof this.props.user !== "undefined"){
            this.setState({user: this.props.user})
            // re-fetch it if the user is now logged in
            this.get_account_data()

            // set teh offer data
            this.set_data(this.state.db, `${this.state.backend}/offers/my`, "data_offers_my", [])
        }

        let user_id_old
        if(prevProps.match && prevProps.match.params && prevProps.match.params.userId){user_id_old = prevProps.match.params.userId}

        let user_id_new
        if( this.props.match && this.props.match.params && this.props.match.params.userId){user_id_new = this.props.match.params.userId}

        if(user_id_old !== user_id_new){
            this.setState({user_id: this.props.user_id})
            // re-fetch it if the user is now logged in
            this.get_account_data()
        }
    }

    componentDidMount = () => {
        let {db} = this.state

        this.get_account_data()

        // get legendary data
        this.set_data(db, `${this.state.backend}/legendary/dropdowns`, "data_dropdown", {})
        // get legendary data
        this.set_data(db, `${this.state.backend}/legendary/item`, "data_legendary", [])
        // get legendary effects
        this.set_data(db, `${this.state.backend}/legendary/effect`, "data_effect", [])
    }

    get_account_data = () =>{
        let {user_id} = this.state
        if(!user_id){ return}

        fetch(`${this.state.backend}/account/${user_id}`, {method: 'GET', credentials: 'include'})
            .then(async (response) => await response.json())
            .then(data => {this.setState({data_user:data})})
            .catch(err => console.log(err))

    }

    display_account_data_header = () =>{
        let {data_user} = this.state
        if(!data_user){ return null}
        if(data_user.account_Data.length ===0){return null}

        let discord
        if(data_user.account_Data[0].discord){
            discord = `${data_user.account_Data[0].discord?.username}#${data_user.account_Data[0].discord?.discriminator}\n<@${data_user.account_Data[0].discord?.id}>`
        }
        let reddit
        if(data_user.account_Data[0].reddit){
            //let name = data_user.account_Data[0].reddit?.name
            //reddit = <a href={`https://www.reddit.com/user/${name}`} target={"_blank"} rel={"noreferrer noopener"} >{name}</a>
            reddit = data_user.account_Data[0].reddit?.name
        }

        return <StructuredListWrapper
            ariaLabel="Structured list"
            style={{"marginBottom": "0px"}}
        >
            <StructuredListHead>
                <StructuredListRow
                    head
                    tabIndex={0}
                >
                    <StructuredListCell head>ID</StructuredListCell>
                    <StructuredListCell head>Fallout76</StructuredListCell>
                    <StructuredListCell head>Steam</StructuredListCell>
                    <StructuredListCell head>Joined</StructuredListCell>
                    <StructuredListCell head>Trades-Buy</StructuredListCell>
                    <StructuredListCell head>Trades-Sell</StructuredListCell>
                    <StructuredListCell head>Discord</StructuredListCell>
                    <StructuredListCell head>Reddit</StructuredListCell>
                </StructuredListRow>
            </StructuredListHead>
            <StructuredListBody>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>{data_user.account_Data[0].id}</StructuredListCell>
                    <StructuredListCell>{data_user.account_Data[0].fo76_name}</StructuredListCell>
                    <StructuredListCell>{data_user.account_Data[0].steam?.displayName}</StructuredListCell>
                    <StructuredListCell>{data_user.account_Data[0].first_added}</StructuredListCell>
                    <StructuredListCell>{data_user.account_Data[0].trades_completed_buy}</StructuredListCell>
                    <StructuredListCell>{data_user.account_Data[0].trades_completed_sell}</StructuredListCell>
                    <StructuredListCell>
                        <pre>{discord}</pre>
                    </StructuredListCell>
                    <StructuredListCell>{reddit}</StructuredListCell>
                </StructuredListRow>
            </StructuredListBody>
        </StructuredListWrapper>
    }

    processTableData = (data_user: Data_User, user_id: string, data_effect: Effect_Object[], data_dropdown: Weapon_Modal_data_dropdown, user?: User,data_offers_my?: Offers_Object[] ): {[propName: string]: JSX.Element | undefined} =>{

        // shared headers
        let headers_tmp_current:{ [propName: string]: DataTableManager_Object_Header } = {
            status:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'status',
                isSortable: true,
                name: 'Status'
            },
            start:{
                id: 'start',
                isSortable: true,
                name: 'Start'
            },
            wanted:{
                filter: {
                    placeholderText: 'Caps/Wishlist',
                    filterFunction: (columnFilterValue:Wanted[], currentValue: any) => {
                        let value = false
                        columnFilterValue.forEach(
                            item => {
                                if(item.type.indexOf(currentValue) !== -1){
                                    value = true
                                }
                            })
                        return value
                    }
                },
                id: 'wanted',
                isSortable: false,
                name: 'Wanted',
                renderDataFunction: (rowData: { value: Wanted[], row: DataTableManager_Object_Row_values }) =>{
                    let result_array: any[] = []
                    let wanted = rowData.value

                    // <a href={`#/user/${rowData.row.seller}`} style={{"textDecoration": "none", color: "#f4f4f4"}} ><b>{rowData.row.seller_name}</b></a>
                    for(let i=0;i<wanted.length;i++){
                        if(wanted[i].type === "caps"){
                            result_array.push(`Caps: ${wanted[i].value}`)
                            result_array.push("\n")
                        }

                        if(wanted[i].type === "wishlist" && wanted[i].value){
                            result_array.push(<a href={`#/user/${rowData.row.seller}/wishlist`} style={{"textDecoration": "none", color: "#f4f4f4"}} ><b>Wishlist</b></a>)
                        }

                    }

                    return <pre>
                        {result_array}
                    </pre>

                }
            },
            note:{
                filter: {
                    placeholderText: 'Note'
                },
                id: 'note',
                isSortable: true,
                name: 'Note'
            },
            level:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'level',
                isSortable: true,
                name: 'Level'
            },
            category:{
                filter: {
                    isMultiselect: true,
                    options: [
                        {id: 'Armor', text: 'Armor'},
                        {id: 'Weapon', text: 'Weapon'}
                    ],
                    placeholderText: 'Search                     '
                },
                id: 'category',
                isSortable: true,
                name: 'Category'
            },
            type:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search                     '
                },
                id: 'type',
                isSortable: true,
                name: 'Type'
            },
            type_sub:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'type_sub',
                isSortable: true,
                name: 'Type-Sub'
            },
            category_specific:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'category_specific',
                isSortable: true,
                name: 'Type-Sub-Sub'
            },
            unique_name:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'unique_name',
                isSortable: true,
                name: 'Unique Name'
            },
            effect_0:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'effect_0',
                isSortable: true,
                name: 'Effect 0'
            },
            effect_1:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'effect_1',
                isSortable: true,
                name: 'Effect 1'
            },
            effect_2:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'effect_2',
                isSortable: true,
                name: 'Effect 2'
            },
            effect_3:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'effect_3',
                isSortable: true,
                name: 'Effect 3'
            },
            /*
            effect_4:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'effect_4',
                isSortable: true,
                name: 'Effect 4'
            },
            effect_5:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'effect_5',
                isSortable: true,
                name: 'Effect 5'
            },
             */
        }

        let headers_tmp_history:{ [propName: string]: DataTableManager_Object_Header } = {
            buyer:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'status',
                isSortable: true,
                name: 'Buyer',
                renderDataFunction: (rowData: { value: Wanted[], row: DataTableManager_Object_Row_values }) =>{
                    let {buyer} = rowData.row.listing
                    if(!buyer){return null}
                    return <a href={`#/user/${buyer.id}`} style={{"textDecoration": "none", color: "#f4f4f4"}} ><b>{buyer.displayName}</b></a>
                }
            },
            start:{
                id: 'start',
                isSortable: true,
                name: 'Start'
            },
            wanted:{
                filter: {
                    placeholderText: 'Caps/Wishlist',
                    filterFunction: (columnFilterValue:Wanted[], currentValue: any) => {
                        let value = false
                        columnFilterValue.forEach(
                            item => {
                                if(item.type.indexOf(currentValue) !== -1){
                                    value = true
                                }
                            })
                        return value
                    }
                },
                id: 'wanted',
                isSortable: false,
                name: 'Wanted',
                renderDataFunction: (rowData: { value: Wanted[], row: DataTableManager_Object_Row_values }) =>{
                    let result_array: any[] = []
                    let wanted = rowData.value

                    // <a href={`#/user/${rowData.row.seller}`} style={{"textDecoration": "none", color: "#f4f4f4"}} ><b>{rowData.row.seller_name}</b></a>
                    for(let i=0;i<wanted.length;i++){
                        if(wanted[i].type === "caps"){
                            result_array.push(`Caps: ${wanted[i].value}`)
                            result_array.push("\n")
                        }

                        if(wanted[i].type === "wishlist" && wanted[i].value){
                            result_array.push(<a href={`#/user/${rowData.row.seller}/wishlist`} style={{"textDecoration": "none", color: "#f4f4f4"}} ><b>Wishlist</b></a>)
                        }

                    }

                    return <pre>
                        {result_array}
                    </pre>

                }
            },
            note:{
                id: 'note',
                isSortable: true,
                name: 'Note'
            },
            level:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'level',
                isSortable: true,
                name: 'Level'
            },
            category:{
                filter: {
                    isMultiselect: true,
                    options: [
                        {id: 'Armor', text: 'Armor'},
                        {id: 'Weapon', text: 'Weapon'}
                    ],
                    placeholderText: 'Search                     '
                },
                id: 'category',
                isSortable: true,
                name: 'Category'
            },
            type:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search                     '
                },
                id: 'type',
                isSortable: true,
                name: 'Type'
            },
            type_sub:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'type_sub',
                isSortable: true,
                name: 'Type-Sub'
            },
            category_specific:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'category_specific',
                isSortable: true,
                name: 'Type-Sub-Sub'
            },
            unique_name:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'unique_name',
                isSortable: true,
                name: 'Unique Name'
            },
            effect_0:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'effect_0',
                isSortable: true,
                name: 'Effect 0'
            },
            effect_1:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'effect_1',
                isSortable: true,
                name: 'Effect 1'
            },
            effect_2:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'effect_2',
                isSortable: true,
                name: 'Effect 2'
            },
            effect_3:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'effect_3',
                isSortable: true,
                name: 'Effect 3'
            },
            /*
            effect_4:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'effect_4',
                isSortable: true,
                name: 'Effect 4'
            },
            effect_5:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'effect_5',
                isSortable: true,
                name: 'Effect 5'
            },
             */
        }
        let headers_tmp_history_bought = Object.assign({}, headers_tmp_history)
        headers_tmp_history_bought.buyer = {
            filter: {
                isMultiselect: true,
                options: [],
                options_tmp: [],
                placeholderText: 'Search'
            },
            id: 'status',
            isSortable: true,
            name: 'Seller',
            renderDataFunction: (rowData: { value: Wanted[], row: DataTableManager_Object_Row_values }) =>{
                let {seller} = rowData.row.listing
                if(!seller){return null}
                return <a href={`#/user/${seller.id}`} style={{"textDecoration": "none", color: "#f4f4f4"}} ><b>{seller.displayName}</b></a>
            }
        }

        let extra_headers_current: DataTableManager_Object_Header[] = []
        // if logged in and user.id === page id then add an edit

        let current_sell_button, current_buy_button
        if(user){
            // filter out all teh cancelled
            if(user_id === user.id){
                extra_headers_current.push(
                    {
                        id: 'edit',
                        isSortable: false,
                        name: 'Edit',
                        renderDataFunction: (rowData: { row: DataTableManager_Object_Row_values }) => {return <WeaponModalEdit listing={rowData.row.listing} setModalValues={(choice: any) => {this.handle_update_listing(choice)}}/>}
                    },
                    {
                        id: 'cancel',
                        isSortable: false,
                        name: 'Cancel',
                        renderDataFunction: (rowData: { row: DataTableManager_Object_Row_values }) => {return <Button kind='danger' size='small' onClick={() => {this.handle_delete_listing(rowData.row.listing.id)}}>Cancel</Button>}
                    },

                )

                // only show buttons on own page
                current_sell_button = <WeaponModal data_dropdown={data_dropdown} data_effect={data_effect} setModalValues={(choice: any)=>{this.handle_new_listing(choice)}}  />
                current_buy_button = <WeaponModal data_dropdown={data_dropdown} data_effect={data_effect} setModalValues={(choice: any)=>{this.handle_new_listing(choice)}} wishlist={true}  />
            }else{
                if(data_offers_my) {
                    extra_headers_current.push({
                        id: 'bid',
                        isSortable: false,
                        name: 'Offer',
                        renderDataFunction: (rowData: any) => {
                            // check if the listing_id is not in teh data_offers_my

                            let existing = data_offers_my.filter(offer => offer.listing.id === rowData.rowId)
                            if (existing.length === 0) {
                                return <Button kind='primary' size='small' onClick={() => {this.offer_make(rowData.rowId)}}>Make Offer</Button>
                            } else {
                                return <Button kind='danger' size='small' onClick={() => {this.offer_delete(rowData.rowId)}}>Cancel Offer</Button>
                            }
                        }
                    })
                }
            }
        }

        let current_sell_table = this.processTableData_general(data_user.current.sell, headers_tmp_current, extra_headers_current)
        let current_buy_table = this.processTableData_general(data_user.current.buy, headers_tmp_current, extra_headers_current)

        let history_sell_table = this.processTableData_general(data_user.history.sell, headers_tmp_history)
        let history_buy_table = this.processTableData_general(data_user.history.buy, headers_tmp_history_bought)

        return {
            current_sell_table: current_sell_table, current_sell_button: current_sell_button,
            current_buy_table: current_buy_table, current_buy_button:current_buy_button,

            history_sell_table: history_sell_table, history_buy_table: history_buy_table,
        }
    }

    processTableData_general = (data_listing: Listing_Object[], headers_tmp:{ [propName: string]: DataTableManager_Object_Header }, extra_headers: DataTableManager_Object_Header[] = []) =>{
        // listings data is always supplied

        // get the legendaries and effects from teh local state
        let {data_legendary, data_effect} = this.state


        let [rows, headers] = process_data_listings_general(headers_tmp,data_listing, data_legendary, data_effect)

        for(let i=0;i<extra_headers.length;i++){
            headers.push(extra_headers[i])
        }

        // standardised view for all on this page
        let view = {
            filters: [],
            pagination: {
                //maxPages: 5,
                page: 1,
                pageSize: 30,
                pageSizes: [
                    10,
                    20,
                    30
                ],
                //totalItems: 100
            },
            table: {
                ordering: [],
                sort: undefined
            },
            toolbar: {
                activeBar: 'filter'
            }
        }

        return <DataTableManager rows={rows} headers={headers} view={view} />
    }

    set_data = async(db: DB, url: string, category: "data_legendary" | "data_effect"| "data_listing" | "data_offers_my" | "data_dropdown" , default_response:any = []) =>{
        //pull initially from cache
        if(category !== "data_dropdown"){
            await db.getAll(category).then(data => this.setState({[category]:data}))
        }

        // fetch fresh data
        await fetch(url, {method: 'GET', credentials: 'include'})
            // convert
            .then(async (response) => await response.json())
            .then(data => {
                // update teh state
                this.setState({[category]:data})
                return data
            })
            .then(async(data: Legendary_Object[] | Effect_Object[] | Listing_Object[] | Offers_Object[])=>{
                if(category === "data_dropdown"){ return}
                if(
                    category === "data_listing" ||
                    category === "data_offers_my"
                ){
                    await db.clear(category)
                }

                for(let i=0;i<data.length;i++){
                    await db.put(category, data[i], data[i].id);
                }
            })
            .catch((err)=> {
                console.log(err);
                this.setState({[category]:default_response})
            })

    }

    handle_new_listing = (choice: any) =>{
        fetch(`${this.state.backend}/listing`, {
            method: 'POST', credentials: 'include',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(choice)
        })
            .then(async response => console.log(await response.json()))
            .then(()=>{
                this.get_account_data()
                this.set_data(this.state.db, `${this.state.backend}/legendary/item`, "data_legendary", [])
            })
            .catch(err => console.log(err))
    }

    handle_update_listing = (choice: any) =>{
        fetch(`${this.state.backend}/listing`, {
            method: 'PATCH', credentials: 'include',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(choice)
        })
            //.then(async response => await response.json()).then(response => console.log("response", response))
            .then(()=>{
                this.get_account_data()
            })
            .catch(err => console.log(err))
    }

    handle_delete_listing = (id: string) =>{
        fetch(`${this.state.backend}/listing`, {
            method: 'PATCH', credentials: 'include',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ listing_id:id, status: "Cancelled"})
        })
            .then(async response => await response.json()).then(response => console.log("response", response))
            .then(()=>{
                this.get_account_data()
            })
            .catch(err => console.log(err))
    }

    offer_make = async (listing_id: string) => {
        fetch(`${this.state.backend}/offers`, {
            method: 'POST', credentials: 'include',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({listing_id: listing_id})
        })
            .then(async response => await response.json())
            .then(result => {
                if(result.success){
                    let {data_offers_my} = this.state
                    let tmp_offer:  Offers_Object = {
                        date: new Date().toISOString(),
                        id: 0,
                        listing: {
                            id: listing_id,
                            item: { md5: "" },
                            start: new Date().toISOString(),
                            status: "Draft",
                            quantity:1,
                            wanted: [],
                            note: ""
                        }
                    }
                    data_offers_my.push(tmp_offer)
                    this.setState({data_offers_my: data_offers_my})
                }
            })
            .catch(err => console.log(err))

    }

    offer_delete = async (listing_id: string) => {
        fetch(`${this.state.backend}/offers`, {
            method: 'DELETE', credentials: 'include',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({listing_id: listing_id})
        })
            .then(async response => await response.json())
            .then(result => {
                if(result.success){
                    this.get_account_data()
                }
            })
            .catch(err => console.log(err))
    }

    offer_accept = async (offer_id: number) => {
        fetch(`${this.state.backend}/offers`, {
            method: 'PATCH', credentials: 'include',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({offer_id: offer_id})
        })
            .then(async response => await response.json())
            .then(result => {
                if(result.success){
                    this.get_account_data()
                }
            })
            .catch(err => console.log(err))
    }

    process_offers_to_table = (
        offers_buy:Offers_Object[],
        offers_sell:Offers_Object[],
        data_legendary: Legendary_Object[],
        data_effect: Effect_Object[]):
        {
            buy_result: DataTableManager_Object_Row[],
            buy_headers: DataTableManager_Object_Header[],
            sell_result: DataTableManager_Object_Row[],
            sell_headers: DataTableManager_Object_Header[]
        } => {

        const headers_shared:{ [propName: string]: DataTableManager_Object_Header } = {
            /*
            id:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'id',
                isSortable: true,
                name: 'Listing ID'
            },
             */
            otherPerson:{
                //  filter: {
                //     placeholderText: 'Sellers'
                // },
                id: 'seller_name',
                isSortable: true,
                name: 'Seller',
                //renderDataFunction: (rowData: any) =>{return <a href={`#/user/${rowData.row.seller}`} style={{"textDecoration": "none", color: "#f4f4f4"}} ><b>{rowData.row.seller_name}</b></a>}
            },
            note:{
                filter: {
                    placeholderText: 'Note'
                },
                id: 'note',
                isSortable: true,
                name: 'Note'
            },
            level:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'level',
                isSortable: true,
                name: 'Level'
            },
            category:{
                filter: {
                    isMultiselect: true,
                    options: [
                        {id: 'Armor', text: 'Armor'},
                        {id: 'Weapon', text: 'Weapon'}
                    ],
                    placeholderText: 'Search                     '
                },
                id: 'category',
                isSortable: true,
                name: 'Category'
            },
            type:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search                     '
                },
                id: 'type',
                isSortable: true,
                name: 'Type'
            },
            type_sub:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'type_sub',
                isSortable: true,
                name: 'Type-Sub'
            },
            category_specific:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'category_specific',
                isSortable: true,
                name: 'Type-Sub-Sub'
            },
            unique_name:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'unique_name',
                isSortable: true,
                name: 'Unique Name'
            },
            effect_0:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'effect_0',
                isSortable: true,
                name: 'Effect 0'
            },
            effect_1:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'effect_1',
                isSortable: true,
                name: 'Effect 1'
            },
            effect_2:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'effect_2',
                isSortable: true,
                name: 'Effect 2'
            },
            effect_3:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'effect_3',
                isSortable: true,
                name: 'Effect 3'
            },
            /*
            effect_4:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'effect_4',
                isSortable: true,
                name: 'Effect 4'
            },
            effect_5:{
                filter: {
                    isMultiselect: true,
                    options: [],
                    options_tmp: [],
                    placeholderText: 'Search'
                },
                id: 'effect_5',
                isSortable: true,
                name: 'Effect 5'
            },
             */

            // this is teh function to eitehr cancel an offer or accept it
            function:{
                id: 'function',
                isSortable: false,
                name: 'Effect 3'
            },
        }

        // stuff you have placed offers in
        let headers_shared_buy = Object.assign({}, headers_shared)
        headers_shared_buy.otherPerson = {
            filter: {
                placeholderText: 'Seller'
            },
            id: 'seller_name',
            isSortable: true,
            name: 'Seller',
            renderDataFunction: (rowData: any) =>{return <a href={`#/user/${rowData.row.seller}`} style={{"textDecoration": "none", color: "#f4f4f4"}} ><b>{rowData.row.seller_name}</b></a>}
        }

        headers_shared_buy.function = {
            id: 'function',
            isSortable: false,
            name: 'Cancel',
            renderDataFunction: (rowData: any) =>{return <Button kind='danger' size='small' onClick={()=>{this.offer_delete(rowData.row.id)}}>Cancel Offer</Button>}
        }

        let headers_shared_sell = Object.assign({}, headers_shared)
        headers_shared_sell.otherPerson = {
            filter: {
                placeholderText: 'Buyer'
            },
            id: 'Buyer_name',
            isSortable: true,
            name: 'Buyer',
            renderDataFunction: (rowData: any) =>{return <a href={`#/user/${rowData.row.buyer}`} style={{"textDecoration": "none", color: "#f4f4f4"}} ><b>{rowData.row.buyer_name}</b></a>}
        }
        headers_shared_sell.function = {
            id: 'function',
            isSortable: false,
            name: 'Accept',
            renderDataFunction: (rowData: any) =>{return <Button kind='danger' size='small' onClick={()=>{this.offer_accept(rowData.rowId)}}>Accept Offer</Button>}
        }

        let [buy_result, buy_headers] = process_offers_to_table_sub(headers_shared_buy,offers_buy, data_legendary, data_effect)
        let [sell_result, sell_headers] = process_offers_to_table_sub(headers_shared_sell,offers_sell, data_legendary, data_effect)

        return {
            buy_result, buy_headers,
            sell_result, sell_headers
        }
    }

    render() {
        let {data_user, user_id, user, data_effect, data_dropdown, data_offers_my, data_legendary} = this.state
        if(!data_user){ return null}
        if(!user_id){return null}
        if(!data_dropdown){return null}

        let {
            current_sell_button, current_sell_table,
            current_buy_button, current_buy_table,
            history_sell_table, history_buy_table,
        } = this.processTableData(data_user, user_id, data_effect, data_dropdown, user,data_offers_my)

        let {buy_headers, buy_result, sell_headers, sell_result} = this.process_offers_to_table(data_user.pending.buy, data_user.pending.sell,data_legendary,  data_effect)

        let tabs = [
            <Tab id="tab-1" label="Current - Sell">
                {current_sell_button}
                {current_sell_table}
            </Tab>,
            <Tab id="tab-2" label="Current - Buy">
                {current_buy_button}
                {current_buy_table}
            </Tab>,
            <Tab id="tab-4" label="History - Sold">
                {history_sell_table}
            </Tab>,
            <Tab id="tab-3" label="History - Bought">
                {history_buy_table}
            </Tab>,
        ]

        if(user && user.id === user_id){
            tabs.push(...[
                <Tab id="tab-6" label="Pending - To Sell">
                    <DataTableManager rows={sell_result} headers={sell_headers}  />
                </Tab>,
                <Tab id="tab-5" label="Pending - To Buy">
                    <DataTableManager rows={buy_result} headers={buy_headers}  />
                </Tab>,
            ])
        }


        return <div className="bx--grid bx--grid--full-width main_body">
            <div className="bx--row main_body__banner">
                <div className="bx--col bx--no-gutter">
                    {this.display_account_data_header()}
                </div>
            </div>
            <div className="bx--row main_body__r2">
                <div className="bx--col bx--no-gutter">
                    <Tabs type='container'>
                        {tabs}
                    </Tabs>
                </div>
            </div>
        </div>
    }
}


interface Weapon_Modal_data_dropdown extends Object {
    categories: {
        Armor: {
            // wood/raider/scout/...
            [propName: string]: {
                Weight: string[]
                // propName is position of legendary
                // string[] is an array of legendary names
                [propName: string]: string[]
            }
        }
        Weapon: {
            // ballistic/energy/...
            [propName: string]: {
                // Pistol/Rifle/ih/2h
                [propName: string]: {
                    // .44/Broadsider/ih/2h
                    // string[] is an array of legendary names
                    [propName: string]: string[]
                }
            }
        }
        [propName: string]: any
    }
    limbs: string[]
    level: number[]
}
interface Weapon_Modal_Props {
    data_dropdown: Weapon_Modal_data_dropdown | undefined
    data_effect: Effect_Object[]

    wishlist?: boolean
    // callback
    setModalValues?:any
}
interface Weapon_Modal_State {
    data_dropdown: Weapon_Modal_data_dropdown | undefined
    data_effect: Effect_Object[]
    choice: Weapon_Modal_choice

    wishlist: boolean
}
interface Weapon_Modal_choice {
    status?: "Draft" | "Active" | "Cancelled" | "Want"
    note?: string

    wanted_caps?: number
    wanted_wishlist?:  boolean

    // weapon specific
    category?: string
    level?: number
    specific?:string
    type?:string
    type_sub?:string
    unique_name?:string
    effect_0?: string
    effect_1?: string
    effect_2?: string
    effect_3?: string
    [propName: string]: any
}
class WeaponModal extends Component <Weapon_Modal_Props, Weapon_Modal_State>{
    constructor(props: Weapon_Modal_Props) {
        super(props);
        this.state = {
            data_dropdown: this.props.data_dropdown,
            data_effect: this.props.data_effect,
            wishlist: this.props.wishlist || false,
            choice: {
                status: undefined,
                note: undefined,

                wanted_caps: undefined,
                wanted_wishlist: true,

                category: undefined,
                level: undefined,
                specific: undefined,
                type: undefined,
                type_sub: undefined,
                unique_name: undefined,
                effect_0: undefined,
                effect_1: undefined,
                effect_2: undefined,
                effect_3: undefined,
            },
        }
    }

    // this updates the component with new values if they are loaded in
    componentDidUpdate(prevProps: Weapon_Modal_Props) {
        if(typeof prevProps.data_dropdown === "undefined"){
            if(typeof this.props.data_dropdown !== "undefined"){
                this.setState({data_dropdown:this.props.data_dropdown })
            }
        }else{
            if(prevProps.data_dropdown.level.length === 0 && typeof this.props.data_dropdown !== "undefined" && this.props.data_dropdown.level.length !== 0){
                this.setState({data_dropdown:this.props.data_dropdown })
            }
        }

        if(prevProps.data_effect.length === 0 && typeof this.props.data_effect !== "undefined" && this.props.data_effect.length !== 0){
            this.setState({data_effect:this.props.data_effect })
        }
    }

    generateDropdown = () => {
        // populate initial data
        ///*
        let {choice, data_dropdown, data_effect, wishlist} = this.state

        let listing_fields: any[] = []
        let weapon_dropdowns = []
        let legendary_effect: { [propName: string]: any[] } = {
            "dropdowns": [],
            "0": [],
            "1": [],
            "2": [],
            "3": [],
            "4": [],
            "5": []
        }


        let status: Submitted_Object["status"][]
        if(wishlist){
            status = ["Want"]
        }else{
            status = ["Active" ,"Draft"]
        }

        let wanted = <StructuredListWrapper ariaLabel="Structured list">
            <StructuredListHead>
                <StructuredListRow head tabIndex={0}>
                    <StructuredListCell head>Caps</StructuredListCell>
                    {
                        /*
                        <StructuredListCell head>Wishlist</StructuredListCell>
                        */
                    }
                </StructuredListRow>
            </StructuredListHead>
            <StructuredListBody>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>
                        <NumberInput
                            id="tj-input"
                            invalidText="Number is not valid"
                            label={"Caps"}
                            //max={100}
                            min={0}
                            step={1000}
                            value={0}
                            onChange={(e: any)=>{
                                let {choice} = this.state
                                choice.wanted_caps = e.target.value
                                this.setState({choice: choice })
                            }}
                        />
                    </StructuredListCell>
                    <StructuredListCell>
                        {
                            /*
                            <StructuredListCell>
                            <ToggleSmall
                                aria-label="toggle button"
                                id="toggle-2"
                                labelText="Item from Wishlist"
                                defaultToggled={this.state.choice.wanted_wishlist}
                                onChange={(e: any)=>{
                                    let {choice} = this.state
                                    let value = e.target.value
                                    if(value === "on"){
                                        choice.wanted_wishlist = true
                                    }else if (value === "off"){
                                        choice.wanted_wishlist = false
                                    }else{
                                        choice.wanted_wishlist = false
                                    }
                                    this.setState({choice: choice })
                                }}
                            />
                        </StructuredListCell>
                             */
                        }
                    </StructuredListCell>
                </StructuredListRow>
            </StructuredListBody>
        </StructuredListWrapper>

        if (!data_dropdown) {return}
        if (!data_effect) {return}

        let {limbs, level} = data_dropdown

        // always available
        level = level || []
        let category = Object.keys(data_dropdown.categories) || []


        let type, type_sub, specific, unique_name

        let type_tmp
        if (typeof choice["category"] !== "undefined") {
            type_tmp = data_dropdown.categories[choice["category"]] || {}
            type = Object.keys(type_tmp) || []
        }

        let type_sub_tmp
        if (typeof choice["type"] !== "undefined") {
            type_sub_tmp = type_tmp[choice["type"]] || {}

            if (choice["category"] === "Armor") {
                type_sub = limbs || []
            } else {
                type_sub = Object.keys(type_sub_tmp) || []
            }
        }


        let specific_tmp
        if (typeof choice["type_sub"] !== "undefined") {
            if (choice["category"] === "Armor") {
                specific_tmp = type_sub_tmp[choice["type_sub"]] || []
                specific = type_sub_tmp.Weight || []
            } else {
                specific_tmp = type_sub_tmp[choice["type_sub"]] || {}
                specific = Object.keys(specific_tmp) || []
            }
        }


        if (typeof choice["specific"] !== "undefined") {
            if (choice["category"] === "Armor") {
                unique_name = specific_tmp
            } else {
                unique_name = specific_tmp[choice["specific"]] || []
            }
        }

        // the item categories are based on teh previous selections
        let category_clear = ["type", "type_sub", "specific", "unique_name", "effect_0", "effect_1", "effect_2", "effect_3"]
        let type_clear = ["type_sub", "specific", "unique_name", "effect_0", "effect_1", "effect_2", "effect_3"]
        let type_sub_clear = ["specific", "unique_name", "effect_0", "effect_1", "effect_2", "effect_3"]
        let specific_clear = ["unique_name", "effect_0", "effect_1", "effect_2", "effect_3"]


        // legendary effects
        if (typeof choice["category"] !== "undefined") {
            // can only choose the dropdown if ye know its
            for (let i = 0; i < data_effect.length; i++) {
                // only adds effects that match teh type armor/weapons
                if (data_effect[i].restriction) {
                    if (data_effect[i].restriction !== choice["category"]) {
                        continue
                    }
                }

                // this gets ranged/melee
                if (data_effect[i].restriction_sub && typeof choice["type"] !== "undefined") {
                    if (data_effect[i].restriction_sub === "Ranged") {
                        if (choice["type"] === "Melee") {
                            continue
                        }
                    } else {
                        // may not need this on teh frontend

                        // if the restriction is melee

                        if (choice["type"] !== "Melee") {
                            continue
                        }
                    }
                }

                // restriction_unique
                let skip = false

                for (let j = 0; j < data_effect[i].restriction_unique.length; j++) {
                    if (skip) {
                        break
                    }

                    let restrictions = data_effect[i].restriction_unique[j]

                    let entry = choice[restrictions.field]
                    if (!entry) {
                        continue
                    }

                    switch (restrictions.selector) {
                        case "cts" : {
                            // unique_name
                            if (entry.indexOf(restrictions.value) === -1) {
                                skip = true
                            }
                            break
                        }
                        case "eq" : {
                            if (entry !== restrictions.value) {
                                skip = true
                            }
                            break
                        }
                    }
                }

                if (skip) {
                    continue
                }

                legendary_effect[data_effect[i].position.toString()].push(data_effect[i].name)
            }
        }


        listing_fields.push(this.generateDropdown_sub(status, "status", "Status", "status"))
        listing_fields.push(
            <TextInput
                //light
                id="test2"
                labelText={false}
                placeholder="Note"
                onChange={(e)=>{
                    let {choice} = this.state
                    choice.note = e.target.value
                    this.setState({choice: choice })
                }}
            />
        )


        weapon_dropdowns.push(this.generateDropdown_sub(level, "level", "Level", "level"))
        weapon_dropdowns.push(this.generateDropdown_sub(category, "category", "Category", "category", category_clear))
        weapon_dropdowns.push(this.generateDropdown_sub(type, "type", "Type", "type", type_clear))
        weapon_dropdowns.push(this.generateDropdown_sub(type_sub, "type_sub", "Subtype", "type_sub", type_sub_clear))
        weapon_dropdowns.push(this.generateDropdown_sub(specific, "specific", "Sub-Subtype", "specific", specific_clear))
        weapon_dropdowns.push(this.generateDropdown_sub(unique_name, "unique_name", "Unique Name", "unique_name"))

        legendary_effect.dropdowns.push(this.generateDropdown_sub(legendary_effect["0"], "effect_0", "Effect 0", "effect_0"))
        legendary_effect.dropdowns.push(this.generateDropdown_sub(legendary_effect["1"], "effect_1", "Effect 1", "effect_1"))
        legendary_effect.dropdowns.push(this.generateDropdown_sub(legendary_effect["2"], "effect_2", "Effect 2", "effect_2"))
        legendary_effect.dropdowns.push(this.generateDropdown_sub(legendary_effect["3"], "effect_3", "Effect 3", "effect_3"))


        const props = () => {
            //const iconToUse = iconMap[select('Icon (icon)', icons, 'none')];
            let edit_submit = this.state.wishlist ? 'New Wishlist' : "New Listing"
            return {
                disabled: false,
                passiveModal: false,
                danger: false,
                buttonTriggerText:edit_submit,
                //buttonTriggerClassName:"bx--btn bx--btn--sm bx--btn--primary",
                hasScrollingContent: true,
                //renderTriggerButtonIcon: typeof iconToUse === 'function' ? iconToUse : undefined,
                //modalLabel: 'Label',
                modalHeading:  `${edit_submit} Options`,
                selectorPrimaryFocus: '[data-modal-primary-focus]',
                primaryButtonText: edit_submit,
                secondaryButtonText: 'Cancel',
                shouldCloseAfterSubmit: true,
                focusTrap: false,
            };
        };

        return <ModalWrapper
            id="transactional-passive-modal"
            handleSubmit={() => {
                this.props.setModalValues(this.process_choice(this.state.choice))
                return true;
            }}
            {...props()}
        >
            Some dropdowns will not be available until some previous ones have been selected
            <hr/>
            <h2>Listing Data</h2>
            {listing_fields}
            <br/>
            <h2>Wanted</h2>
            <h4>Set a caps value here if ye wish</h4>
            {wanted}

            <h2>Item Data</h2>
            {weapon_dropdowns}
            <br/>
            <h2>Effects</h2>
            {legendary_effect.dropdowns}
        </ModalWrapper>

    }

    generateDropdown_sub = (data: any[] = [], id: string, label: string, field: string, clear: string[] = [], titleText: string = "") =>{
        let dropdown_choice_initial = this.state.choice
        let selected_item = typeof dropdown_choice_initial[id] === "undefined" ? label : dropdown_choice_initial[id]

        return <Dropdown
            selectedItem={selected_item}
            ariaLabel="DropdownModal"
            id={`Dropdown_${id}`}
            type="default"
            titleText={titleText}
            label={"label"}
            items={data.sort().sort((a, b) => a - b)}
            itemToString={(item) => (item ? item : '')}
            onChange={(result)=>{
                let {choice} = this.state
                choice[id] = result.selectedItem as string
                for(let i=0;i<clear.length;i++){
                    choice[clear[i]] = undefined
                }

                this.setState({choice: choice })
            }}
        />
    }

    process_choice = (choice: Weapon_Modal_choice) =>{
        let status: Submitted_Object["status"]
        if(typeof choice.status === "undefined"){
            status = this.state.wishlist ? 'Want' : "Draft"
        }else{
            status = choice.status
        }

        let result: Submitted_Object = {
            // quantity is always 1
            quantity: 1,
            status:status,
            wanted: [],
            note: choice.note,

            category: choice.category,
            level: choice.level as number,
            specific:choice.specific,
            type:choice.type,
            type_sub:choice.type_sub,
            unique_name:choice.unique_name,
            selected_effects: []
        }

        if(result.selected_effects){
            if(choice.effect_0){result.selected_effects.push(choice.effect_0)}
            if(choice.effect_1){result.selected_effects.push(choice.effect_1)}
            if(choice.effect_2){result.selected_effects.push(choice.effect_2)}
            if(choice.effect_3){result.selected_effects.push(choice.effect_3)}
        }

        if(result.wanted){
            if(choice.wanted_caps){result.wanted.push({type: "caps", value: choice.wanted_caps})}
            // does not get added if false
            if(choice.wanted_wishlist){result.wanted.push({type: "wishlist", value: choice.wanted_wishlist})}
        }

        return result
    }

    render() {
        if(!this.state.data_effect || !this.state.data_dropdown){return null}

        return this.generateDropdown()
    }
}

interface WeaponModalEdit_Props {
    listing: Listing_Object

    // callback
    setModalValues?:any
}
interface WeaponModalEdit_State {
    listing: Listing_Object

    choice: WeaponModalEdit_choice
}
interface WeaponModalEdit_choice {
    status: "Draft" | "Active" | "Cancelled" | "Want"
    note: string

    wanted_caps?: number
    wanted_wishlist?:  boolean

    [propName: string]: any
}
class WeaponModalEdit extends Component <WeaponModalEdit_Props, WeaponModalEdit_State>{
    constructor(props: WeaponModalEdit_Props) {
        super(props);

        this.state = {
            listing: this.props.listing,
            choice: {
                status: this.props.listing.status,
                note: this.props.listing.note,

                wanted_caps: undefined,
                wanted_wishlist: true,
            },
        }
    }

    generateDropdown = () => {
        // populate initial data
        ///*
        let {listing} = this.state

        let listing_fields: any[] = []

        let status: Submitted_Object["status"][]
        if(listing.status === "Want"){
            status = ["Want"]
        }else{
            status = ["Active" ,"Draft", "Cancelled"]
        }

        let initial_caps = 0
        let want_caps = listing.wanted.filter(item => item.type === "caps")
        if(want_caps.length >0){
            initial_caps = want_caps[0].value
        }


        let wanted = <StructuredListWrapper ariaLabel="Structured list">
            <StructuredListHead>
                <StructuredListRow head tabIndex={0}>
                    <StructuredListCell head>Caps</StructuredListCell>
                    {
                        /*
                        <StructuredListCell head>Wishlist</StructuredListCell>
                        */
                    }
                </StructuredListRow>
            </StructuredListHead>
            <StructuredListBody>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>
                        <NumberInput
                            id="tj-input"
                            invalidText="Number is not valid"
                            label={"Caps"}
                            //max={100}
                            min={0}
                            step={1000}
                            value={initial_caps}
                            onChange={(e: any)=>{
                                let {choice} = this.state
                                choice.wanted_caps = e.target.value
                                this.setState({choice: choice })
                            }}
                        />
                    </StructuredListCell>
                    {
                        /*
                        <StructuredListCell>
                        <ToggleSmall
                            aria-label="toggle button"
                            id="toggle-2"
                            labelText="Item from Wishlist"
                            defaultToggled={this.state.choice.wanted_wishlist}
                            onChange={(e: any)=>{
                                let {choice} = this.state
                                let value = e.target.value
                                if(value === "on"){
                                    choice.wanted_wishlist = true
                                }else if (value === "off"){
                                    choice.wanted_wishlist = false
                                }else{
                                    choice.wanted_wishlist = false
                                }
                                this.setState({choice: choice })
                            }}
                        />
                    </StructuredListCell>
                         */
                    }
                </StructuredListRow>
            </StructuredListBody>
        </StructuredListWrapper>


        listing_fields.push(this.generateDropdown_sub(status, "status", "Status", "status"))
        listing_fields.push(
            <TextInput
                id="test2"
                labelText={false}
                value={this.state.choice.note}
                onChange={(e)=>{
                    let {choice} = this.state
                    choice.note = e.target.value
                    this.setState({choice: choice })
                }}
            />
        )

        const props = () => {
            //const iconToUse = iconMap[select('Icon (icon)', icons, 'none')];
            let edit_submit = listing.status === "Want" ? 'Update Wishlist' : "Update Listing"
            return {
                disabled: false,
                passiveModal: false,
                danger: false,
                buttonTriggerText:edit_submit,
                buttonTriggerClassName:"bx--btn bx--btn--sm bx--btn--primary",
                hasScrollingContent: true,
                //renderTriggerButtonIcon: typeof iconToUse === 'function' ? iconToUse : undefined,
                //modalLabel: 'Label',
                modalHeading:  `${edit_submit} Options`,
                selectorPrimaryFocus: '[data-modal-primary-focus]',
                primaryButtonText: edit_submit,
                secondaryButtonText: 'Cancel',
                shouldCloseAfterSubmit: true,
                focusTrap: false,
            };
        };

        return <ModalWrapper
            id="transactional-passive-modal"
            handleSubmit={() => {
                this.props.setModalValues(this.process_choice(this.state.choice))
                return true;
            }}
            {...props()}
        >
            Some dropdowns will not be available until some previous ones have been selected
            <hr/>
            <h2>Listing Data</h2>
            {listing_fields}
            <br/>
            <h2>Wanted</h2>
            <h4>Set a caps value here if ye wish</h4>
            {wanted}
        </ModalWrapper>

    }

    generateDropdown_sub = (data: any[] = [], id: string, label: string, field: string, clear: string[] = [], titleText: string = "") =>{
        let dropdown_choice_initial = this.state.choice
        let selected_item = typeof dropdown_choice_initial[id] === "undefined" ? label : dropdown_choice_initial[id]

        return <Dropdown
            selectedItem={selected_item}
            ariaLabel="DropdownModal"
            id={`Dropdown_${id}`}
            type="default"
            titleText={titleText}
            label={"label"}
            items={data.sort().sort((a, b) => a - b)}
            itemToString={(item) => (item ? item : '')}
            onChange={(result)=>{
                let {choice} = this.state
                choice[id] = result.selectedItem as string
                for(let i=0;i<clear.length;i++){
                    choice[clear[i]] = undefined
                }

                this.setState({choice: choice })
            }}
        />
    }

    process_choice = (choice: WeaponModalEdit_choice) =>{
        let result: Submitted_Object = {
            // quantity is always 1
            listing_id: this.state.listing.id,
            quantity: 1,
            status:choice.status,
            wanted: [],
            note: choice.note,
        }

        if(result.wanted){
            if(choice.wanted_caps){result.wanted.push({type: "caps", value: choice.wanted_caps})}
            // does not get added if false
            if(choice.wanted_wishlist){result.wanted.push({type: "wishlist", value: choice.wanted_wishlist})}
        }

        return result
    }

    render() {
        if(!this.state.listing){return null}

        return this.generateDropdown()
    }
}

function process_offers_to_table_sub (
    headers_tmp:{ [propName: string]: DataTableManager_Object_Header },
    offers:Offers_Object[],
    data_legendary: Legendary_Object[],
    data_effect: Effect_Object[]
): [DataTableManager_Object_Row[], DataTableManager_Object_Header[]] {
    let result: DataTableManager_Object_Row[] = []

    let headers_tmp_keys = Object.keys(headers_tmp)

    offers.forEach(offer => {
        let {note, item, wanted, status} = offer.listing
        let legendary = item.md5
        let row_item: DataTableManager_Object_Row = {
            // listing data
            id: offer.id.toString(),
            values: {
                id: offer.listing.id,
                offer: offer,
                listing: offer.listing,
                platform: "",

                seller: offer.user_seller?.id || "",
                seller_name: offer.user_seller?.displayName || "",

                buyer: offer.user_buyer?.id || "",
                buyer_name: offer.user_buyer?.displayName || "",

                start_timestamp: offer.date,
                start: timeAgo(Date.now() - new Date(offer.date).getTime()),


                wanted: wanted,
                note: note,
                status: status,

                // weapon data
                level: "",
                category: "",
                type: "",
                type_sub: "",
                category_specific: "",
                unique_name: "",
                // effect specific
                effect_0: "",
                effect_1: "",
                effect_2: "",
                effect_3: "",
                effect_4: "",
                effect_5: "",
            }

        }

        if (data_legendary.length > 0) {
            let item_data = data_legendary.filter(item => item.md5 === legendary)
            if (item_data.length > 0) {
                let item = item_data[0]
                row_item.values.level = item.level.toString()
                row_item.values.category = item.category
                row_item.values.type = item.type
                row_item.values.type_sub = item.type_sub
                row_item.values.category_specific = item.category_specific
                row_item.values.unique_name = item.unique_name

                // now legendary effects
                if (data_effect.length > 0) {
                    row_item.values.effect_0 = data_effect.filter(effect => effect.id === item.effect_0).map(effect => effect.name)[0] || ""
                    row_item.values.effect_1 = data_effect.filter(effect => effect.id === item.effect_1).map(effect => effect.name)[0] || ""
                    row_item.values.effect_2 = data_effect.filter(effect => effect.id === item.effect_2).map(effect => effect.name)[0] || ""
                    row_item.values.effect_3 = data_effect.filter(effect => effect.id === item.effect_3).map(effect => effect.name)[0] || ""
                    row_item.values.effect_4 = data_effect.filter(effect => effect.id === item.effect_4).map(effect => effect.name)[0] || ""
                    row_item.values.effect_5 = data_effect.filter(effect => effect.id === item.effect_5).map(effect => effect.name)[0] || ""
                }
            }
        }

        // generate headers here
        for (let j = 0; j < headers_tmp_keys.length; j++) {
            let entry = headers_tmp[headers_tmp_keys[j]]

            if (typeof entry.filter === "undefined") {continue}
            if (typeof entry.filter.options === "undefined") {continue}
            if (typeof entry.filter.options_tmp === "undefined") {continue}

            let value = row_item.values[headers_tmp_keys[j]]//.toString()
            if (!value) {continue}
            if (entry.filter.options_tmp.indexOf(value) !== -1) {continue}

            let filter_object = {
                id: value,
                text: value
            }

            // @ts-ignore
            headers_tmp[headers_tmp_keys[j]].filter.options.push(filter_object)
            // @ts-ignore
            headers_tmp[headers_tmp_keys[j]].filter.options_tmp.push(value)
        }

        result.push(row_item)


    })

    result.sort((a, b)=> compare(a,b, "start_timestamp")).reverse()

    // break out servers into their own array
    let headers = Object.values(headers_tmp)

    return [result, headers]
}