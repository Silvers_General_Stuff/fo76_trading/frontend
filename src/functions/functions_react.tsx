import React, {Component} from "react";
// @ts-ignore
import { StatefulTable } from 'carbon-addons-iot-react';
import {compare, timeAgo} from "./functions";
import {Effect_Object, Legendary_Object, Listing_Object} from "../interfaces/interfaces";




interface DataTableManager_Props {
    rows: DataTableManager_Object_Row[];
    headers: DataTableManager_Object_Header[];
    view?: DataTableManager_View
    options?: DataTableManager_Options
    style?: Object
}
interface DataTableManager_State {
    rows: DataTableManager_Object_Row[];
    headers: DataTableManager_Object_Header[]
}

export interface DataTableManager_Object_Row extends Object {
    id: string
    values: DataTableManager_Object_Row_values
}

export interface DataTableManager_Object_Row_values extends Object {
    listing:  Listing_Object
    seller_name: string
    seller:string
    platform: string
    start_timestamp: string
    start: string
    wanted: Object[],
    note: string
    status: string
    // weapon data
    level: string,
    category: string
    type: string
    type_sub: string
    category_specific: string
    unique_name: string
    // effect specific
    effect_0: string
    effect_1: string
    effect_2: string
    effect_3: string
    effect_4: string
    effect_5: string

    [propName: string]: any
}

export interface DataTableManager_Object_Header extends Object {
    id: string;
    name: string;
    isSortable: boolean;
    filter?: {
        isMultiselect?: boolean;
        options?: any[];
        options_tmp?: any[];
        placeholderText?: string;
        filterFunction?: any
    };
    renderDataFunction?:any;
}

export interface DataTableManager_View extends Object {
    filters?: any[]
    pagination?: {
        maxPages?: number
        page?: number,
        pageSize?: number,
        pageSizes?: number[],
        totalItems?: number
    }
    table?: {
        ordering?: any[],
        sort?: undefined
    },
    toolbar?: {
        activeBar?: 'filter' | string
    }
}

export interface DataTableManager_Options extends Object {
    hasPagination?: boolean
    hasRowSelection?: 'multi' | 'single' | false,
    hasRowExpansion?: boolean
    hasRowNesting?: boolean
    hasRowActions?: boolean
    hasFilter?: boolean
    hasOnlyPageData?: boolean,
    hasSearch?: boolean,
    hasColumnSelection?: boolean,
    hasColumnSelectionConfig?: boolean,
    shouldLazyRender?: boolean,
    hasRowCountInHeader?: boolean,
    hasResize?: boolean,
    hasSingleRowEdit?: boolean,
    hasUserViewManagement?: boolean,
    useAutoTableLayoutForResize?: boolean,
    wrapCellText?: 'always' | 'never' | 'auto',
}

export class DataTableManager extends Component  <DataTableManager_Props, DataTableManager_State> {
    constructor(props: DataTableManager_Props) {
        super(props);
        this.state = {
            rows: this.props.rows || [],
            headers: this.props.headers || [],
        }
    }

    build_table = () => {
        let default_view = {
            filters: [],
            pagination: {
                //maxPages: 5,
                page: 1,
                pageSize: 20,
                pageSizes: [
                    10,
                    20,
                    30
                ],
                //totalItems: 100
            },
            table: {
                ordering: [],
                sort: undefined
            },
            toolbar: {
                activeBar: 'filter'
            }
        }

        let default_options: DataTableManager_Options = {
            hasPagination: true,
            wrapCellText: 'auto',
        }

        return <StatefulTable
            columns={this.props.headers}
            data={this.props.rows}
            id="table"
            isSortable
            options={this.props.options || default_options}
            view={this.props.view || default_view}
            style={this.props.style}
        />

    }

    render() {
        return this.build_table()
    }
}

export const process_data_listings_general = (
    headers_tmp:{ [propName: string]: DataTableManager_Object_Header },
    data_listing: Listing_Object[] ,
    data_legendary: Legendary_Object[],
    data_effect: Effect_Object[]
): [DataTableManager_Object_Row[], DataTableManager_Object_Header[]] =>{

    // this is what gets returned later
    let result: DataTableManager_Object_Row[] = []

    let headers_tmp_keys = Object.keys(headers_tmp)
    for(let i=0;i<data_listing.length;i++){
        let {note, id, start, wanted, status, seller, item } = data_listing[i]
        if(!seller){continue}

        let legendary = item.md5

        // (Date.now() - happenedAtMs)
        let row_item:DataTableManager_Object_Row = {
            // listing data
            id: id,
            values:{
                listing: data_listing[i],
                seller_name: seller.displayName,
                seller:seller.id,
                platform: seller.platform || "",
                start_timestamp: start,
                start: timeAgo(Date.now() - new Date(start).getTime()),
                wanted: wanted,
                note: note,
                status: status,
                // weapon data
                level: "",
                category: "",
                type: "",
                type_sub: "",
                category_specific: "",
                unique_name: "",
                // effect specific
                effect_0: "",
                effect_1: "",
                effect_2: "",
                effect_3: "",
                effect_4: "",
                effect_5: "",
            }
        }
        // now fill in teh weapon data
        if(data_legendary.length > 0){
            let item_data = data_legendary.filter(item => item.md5 === legendary)
            if(item_data.length > 0){
                let item = item_data[0]
                row_item.values.level = item.level.toString()
                row_item.values.category = item.category
                row_item.values.type = item.type
                row_item.values.type_sub = item.type_sub
                row_item.values.category_specific = item.category_specific
                row_item.values.unique_name = item.unique_name

                // now legendary effects
                if(data_effect.length > 0){
                    row_item.values.effect_0 = data_effect.filter(effect => effect.id === item.effect_0).map(effect => effect.name)[0] || ""
                    row_item.values.effect_1 = data_effect.filter(effect => effect.id === item.effect_1).map(effect => effect.name)[0] || ""
                    row_item.values.effect_2 = data_effect.filter(effect => effect.id === item.effect_2).map(effect => effect.name)[0] || ""
                    row_item.values.effect_3 = data_effect.filter(effect => effect.id === item.effect_3).map(effect => effect.name)[0] || ""
                    row_item.values.effect_4 = data_effect.filter(effect => effect.id === item.effect_4).map(effect => effect.name)[0] || ""
                    row_item.values.effect_5 = data_effect.filter(effect => effect.id === item.effect_5).map(effect => effect.name)[0] || ""
                }
            }
        }

        // generate headers here
        for(let j=0;j<headers_tmp_keys.length;j++){
            let entry = headers_tmp[headers_tmp_keys[j]]

            if(typeof entry.filter === "undefined"){continue}
            if(typeof entry.filter.options === "undefined"){continue}
            if(typeof entry.filter.options_tmp === "undefined"){continue}

            let value = row_item.values[headers_tmp_keys[j]]//.toString()
            if(!value){continue}
            if(entry.filter.options_tmp.indexOf(value) !== -1){continue}

            let filter_object = {
                id: value,
                text: value
            }

            // @ts-ignore
            headers_tmp[headers_tmp_keys[j]].filter.options.push(filter_object)
            // @ts-ignore
            headers_tmp[headers_tmp_keys[j]].filter.options_tmp.push(value)
        }

        result.push(row_item)
    }

    result.sort((a, b)=> compare(a,b, "start_timestamp")).reverse()

    // break out servers into their own array
    let headers = Object.values(headers_tmp)

    return [result, headers]
}