import {DBSchema, IDBPDatabase} from "idb";

// user
export interface User extends Object {
    id: string;
    displayName: string
    photos: object[]
    first_added: Date
    trades_completed:number
}

// database stuff
export interface DB extends IDBPDatabase<LocalCached> {}
export interface LocalCached extends DBSchema {
    data_legendary: {
        key: string;
        value: Legendary_Object;
        indexes: { 'id': string };
    };
    data_effect: {
        key: string;
        value: Effect_Object;
        indexes: { 'id': string };
    };
    data_listing: {
        key: string;
        value: Listing_Object;
        indexes: { 'id': string };
    };
    data_offers_my: {
        key: number;
        value: Offers_Object;
        indexes: { 'id': number };
    };
}

// legendaries
export interface Legendary_Object extends Object {
    id: string
    md5: string
    first_added: Date
    unique_name: string
    level: number
    category: string
    type: string
    type_sub: string
    category_specific: string
    effect_0?: string;
    effect_1?: string;
    effect_2?: string;
    effect_3?: string;
    effect_4?: string;
    effect_5?: string;
}

// effects
export interface Effect_Object extends Object {
    id: string
    name: string
    position: number
    description: string
    restriction: string
    restriction_sub: string
    restriction_unique: Effect_Object_restriction_unique[]
}
export interface Effect_Object_restriction_unique extends Object {
    field: string
    selector: string
    value: string
}

// listings
export interface Listing_Object extends Object {
    id: string
    start: string
    status: "Draft" | "Active" | "Cancelled" | "Want"
    item: { md5: string }
    quantity: number
    wanted: Wanted[]
    note: string
    buyer?: {id: string; displayName: string}
    seller?: {id: string; displayName: string; platform?: string}
}

// offers
export interface Offers_Object {
    id: number
    date: string
    user_buyer?: {
        id: string
        displayName: string
    }
    user_seller?: {
        id: string
        displayName: string
    }
    listing:Listing_Object
}

// wanted
export interface Wanted {
    // item (ammo/scrap/flux) /
    // Legendary / Item / Currency / List
    type: string
    value: any
}

// what gets sent to the server for each legendary
export interface Submitted_Object {
    // listing specific
    quantity: number
    status: "Draft" | "Active" | "Cancelled" | "Want"
    wanted?: Wanted[]
    note?: string
    listing_id?: string

    // weapon specific
    category?: string
    level?: number
    specific?:string
    type?:string
    type_sub?:string
    unique_name?:string
    selected_effects?: string[]
}

export interface Data_User_Account_Data {
    // basic data
    id: string
    first_added: string
    displayName: string

    // platform specific
    platform: string
    fo76_name: string
    fo76_names_past: string[]

    // steam only
    has_game: boolean

    // trade data
    trades_completed: number
    trades_completed_buy: number
    trades_completed_sell: number

    steam: {
        displayName: string
        first_added?: string
    }
    discord?: {
        username: string;
        id: string;
        discriminator: string
        first_added?: string
    } | null
    reddit?: {
        name: string
        first_added?: string
    } | null
}

export interface ExtendedWindow extends Window {
    _env_?: any;
}