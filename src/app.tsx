import React, {Component} from "react";
import {
    HashRouter,
    Switch,
    Route
} from "react-router-dom";
import cookie from 'react-cookies'

import {Login} from "./content/logged_in/login"
import {Account} from "./content/logged_in/account"
import { Listings } from "./content/logged_out/listings"
import {UserData} from "./content/logged_out/user"
import {Home} from "./content/logged_out/home"
import {
    HeaderContainer,
    Header,
    SkipToContent,
    HeaderMenuButton,
    HeaderName,
    HeaderNavigation,
    HeaderMenuItem,
    SideNav,
    SideNavItems,
    HeaderSideNavItems,
    Content
} from 'carbon-components-react';
import { openDB } from "idb";

// interfaces
import {User, DB, LocalCached, ExtendedWindow} from "./interfaces/interfaces"

async function liftDb(): Promise<DB> {
    return await openDB<LocalCached>("LocalCached", 1, {
        async upgrade(db) {
            await Promise.all([
                db.createObjectStore("data_legendary").createIndex("id", "id"),
                db.createObjectStore("data_effect").createIndex("id", "id"),
                db.createObjectStore("data_listing").createIndex("id", "id"),
                db.createObjectStore("data_offers_my").createIndex("id", "id"),
            ])
            return db
        },
    });
}

interface App_State {
    user?: User;
    db?: DB,
    backend?: string
}
export class App extends Component <any, App_State>{
    constructor(props: any) {
        super(props);
        let backend
        let {_env_}: ExtendedWindow = window
        if(_env_ && _env_.REACT_APP_backend_address){
            backend = _env_.REACT_APP_backend_address
        }
        this.state = {
            user: undefined,
            db: undefined,
            backend: backend
        }
    }

    handleUser = (user:User) =>{
        if(typeof this.state.user === "undefined"){
            this.setState({user:user})
        }
    }

    // if the jwt exists and user is undefined then fetch it
    getUser = async() =>{
        let url = `${this.state.backend}/account`
        let result: Response = await fetch(url, {
            method: 'GET',
            credentials: 'include'
        })
            .catch((err)=> {console.log(err);return err})

        if(result.status === 200){
            let user_tmp = await result.json()
            if(user_tmp.length !== 0){
                this.setState({user: user_tmp[0]})
            }
        }
    }

    componentDidMount = async () => {
        // first set up the db conenction
        this.setState({db:await liftDb()})

        // then if the user is logged in get teh user data
        if(cookie.load("loggedIn") && typeof this.state.user === "undefined"){
            await this.getUser()
        }
    }


    render() {
        let {db, user, backend} = this.state
        // check to see if db is initialised
        if(typeof db === "undefined"){return null}
        console.log(backend)

        let nav_links = [
            <HeaderMenuItem href="#/">Home</HeaderMenuItem>,
            <HeaderMenuItem href="#/listings">Listings</HeaderMenuItem>,
        ]

        if(user){
            nav_links.push(<HeaderMenuItem href="#/account">Account</HeaderMenuItem>)
            nav_links.push(<HeaderMenuItem href={`#/user/${user.id}`}>Profile</HeaderMenuItem>)
        }else{
            nav_links.push(<HeaderMenuItem href="#/login">Login</HeaderMenuItem>)
        }

        let navbar = <HeaderContainer
            render={({ isSideNavExpanded, onClickSideNavExpand }) => (
                <Header aria-label="Fo76.ie">
                    <SkipToContent />
                    <HeaderMenuButton
                        aria-label="Open menu"
                        onClick={onClickSideNavExpand}
                        isActive={isSideNavExpanded}
                    />
                    <HeaderName href="#" prefix="">Fo76.ie</HeaderName>
                    <HeaderNavigation aria-label="Fo76.ie">
                        {nav_links}
                    </HeaderNavigation>
                    <SideNav
                        aria-label="Side navigation"
                        expanded={isSideNavExpanded}
                        isPersistent={false}>
                        <SideNavItems>
                            <HeaderSideNavItems>
                                {nav_links}
                            </HeaderSideNavItems>
                        </SideNavItems>
                    </SideNav>
                </Header>
            )}
        />

        let routes = [
            <Route path="/listings"><Listings user={this.state.user} db={db} backend={backend} /></Route>,
            <Route path="/login" ><Login setUser={this.handleUser} backend={backend} /></Route>,
            <Route path="/account" ><Account user={this.state.user} db={db} backend={backend} /></Route>,
            <Route exact path="/user/:userId" render={(props) => {if(typeof db === "undefined"){return null} return <UserData {...props} user={user} db={db} backend={backend} />}}/>,
            <Route exact path="/user/:userId/:section" render={(props) => {if(typeof db === "undefined"){return null}return <UserData {...props} user={user} db={db} backend={backend} />}}/>,
            <Route path="/"><Home /></Route>,
        ]

        return <HashRouter>
            <div>
                {navbar}
                <Content>
                    <Switch>
                        {routes}
                    </Switch>
                </Content>
            </div>
        </HashRouter>
    }
}