// core of react
import React from 'react';
import ReactDOM from 'react-dom';

// core file that manages everything
import {App} from "./app"

// css to be used
import "./css/custom.scss"
import "./css/index.scss"

// for PWA
//import * as serviceWorker from './serviceWorker';

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
//serviceWorker.unregister();