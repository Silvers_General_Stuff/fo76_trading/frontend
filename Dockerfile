# https://medium.com/better-programming/production-ready-react-with-docker-and-travis-ci-8e2ae559f26c

# build stage
FROM node:12 as build_stage

# to stop OOM issue with react-scripts
ENV GENERATE_SOURCEMAP=false

# building teh frontend
WORKDIR /app
COPY package.json /app
COPY yarn.lock /app

RUN yarn
COPY . /app
RUN yarn build


# serving teh content
FROM caddy:2-alpine
COPY ./docker/Caddyfile /etc/caddy/Caddyfile
COPY --from=build_stage /app/build /usr/share/caddy/html
EXPOSE 8080

WORKDIR /usr/share/caddy/html
# manage the env
COPY ./docker/env.sh .
COPY ./docker/.env .
# Add bash
RUN apk add --no-cache bash
# Make our shell script executable
RUN chmod +x ./env.sh
# runs whrn teh container loads, this will create teh env.js file

CMD /bin/bash -c ./env.sh && caddy run --config /etc/caddy/Caddyfile --adapter caddyfile
#CMD [ "/bin/bash", "-c", "./env.sh", " && ",  "caddy", "run", "--config", "/etc/caddy/Caddyfile", "--adapter", "caddyfile" ]

